<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpPresentation\Style\Color;
use PhpOffice\PhpPresentation\Style\Alignment;
use PhpOffice\PhpPresentation\Style\Border;

use PhpOffice\PhpPresentation\Shape\Chart\Type\Area;
use PhpOffice\PhpPresentation\Shape\Chart\Series;

use PhpOffice\PhpPresentation\Slide\Transition;


class ODPController extends Controller
{
  public function index()
  {
    global $oFill;
    global $oShadow;

    $objPHPPowerPoint = new PhpPresentation();

    // Create slide
    $slide0 = $objPHPPowerPoint->getActiveSlide();

    $seriesData = array(
      'Monday' => 12,
      'Tuesday' => 15,
      'Wednesday' => 13,
      'Thursday' => 17,
      'Friday' => 14,
      'Saturday' => 9,
      'Sunday' => 7
    );

    // Create a line chart (that should be inserted in a shape)
    $areaChart = new Area();
    $series = new Series('Downloads', $seriesData);
    $series->setShowSeriesName(true);
    $series->setShowValue(true);
    $series->getFill()->setStartColor(new Color('FF93A9CE'));
    $series->setLabelPosition(Series::LABEL_INSIDEEND);
    $areaChart->addSeries($series);

    // Create a shape (chart)
    $shape = $slide0->createChartShape();
    $shape->getTitle()->setVisible(false);
    $shape->setName('PHPPresentation Daily Downloads')->setResizeProportional(false)->setHeight(550)->setWidth(700)->setOffsetX(120)->setOffsetY(80);
    $shape->getBorder()->setLineStyle(Border::LINE_SINGLE);
    $shape->getTitle()->setText('PHPPresentation Daily Downloads');
    $shape->getTitle()->getFont()->setItalic(true);
    $shape->getPlotArea()->setType($areaChart);
    $shape->getPlotArea()->getAxisX()->setTitle('Axis X');
    $shape->getPlotArea()->getAxisY()->setTitle('Axis Y');
    $shape->getView3D()->setRotationX(30);
    $shape->getView3D()->setPerspective(30);
    $shape->getLegend()->getBorder()->setLineStyle(Border::LINE_SINGLE);
    $shape->getLegend()->getFont()->setItalic(true);

    $shape->getShadow()->setVisible(true)
                       ->setDirection(45)
                       ->setDistance(10);

    $oTransition = new Transition();
    $oTransition->setManualTrigger(false);
    $oTransition->setTimeTrigger(true, 4000);
    $oTransition->setTransitionType(Transition::TRANSITION_SPLIT_IN_VERTICAL);
    $slide0->setTransition($oTransition);

    // Create a shape (text)
    $slide1 = $objPHPPowerPoint->createSlide();
    $shape = $slide1->createRichTextShape()
                    ->setHeight(300)
                    ->setWidth(600)
                    ->setOffsetX(170)
                    ->setOffsetY(180);
    $shape->getActiveParagraph()->getAlignment()->setHorizontal( Alignment::HORIZONTAL_CENTER );
    $textRun = $shape->createTextRun('Latech Demo!');
    $textRun->getFont()->setBold(true)
                       ->setSize(60)
                       ->setColor( new Color( 'FFE06B20' ) );

    $oWriterODP = IOFactory::createWriter($objPHPPowerPoint, 'ODPresentation');
    $fileName = $this->getParameter('presentations_dir') . "/sample.odp" ;
    $oWriterODP->save($fileName);

    return $this->file($fileName);
  }
}
